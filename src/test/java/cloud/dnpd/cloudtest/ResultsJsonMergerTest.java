package cloud.dnpd.cloudtest;

import cloud.dnpd.codetest.Answer;
import cloud.dnpd.codetest.Results;
import cloud.dnpd.codetest.ResultsJsonMerger;
import cloud.dnpd.codetest.jsonfile.RawExam;
import cloud.dnpd.codetest.jsonfile.RawOption;
import cloud.dnpd.codetest.jsonfile.RawQuestion;
import cloud.dnpd.codetest.jsonfile.RawResponse;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class ResultsJsonMergerTest {

    private final UUID questionId = UUID.randomUUID();

    @Test
    public void testSquishA() {
        ResultsJsonMerger merger = new ResultsJsonMerger();

        Results resultsForA = merger.squishIntoResults(exam(), response("A"));

        Answer answer = resultsForA.getAnswers().get(0);
        assertThat(resultsForA.getPercentageOfMax()).isEqualTo(100);
        assertThat(answer.getPoints()).isEqualTo(3);
        assertThat(answer.getCorrect()).isTrue();
        assertThat(answer.getFeedback()).isEqualTo("some feedback");
        assertThat(answer.getBestAnswerLabel()).isEqualTo("A");
        assertThat(answer.getAnswerLabel()).isEqualTo("A");
    }

    @Test
    public void testSquishB() {
        ResultsJsonMerger merger = new ResultsJsonMerger();

        Results resultsForA = merger.squishIntoResults(exam(), response("B"));

        Answer answer = resultsForA.getAnswers().get(0);
        assertThat(resultsForA.getPercentageOfMax()).isEqualTo(33);
        assertThat(answer.getPoints()).isEqualTo(1);
        assertThat(answer.getCorrect()).isTrue();
        assertThat(answer.getFeedback()).isEqualTo("some feedback for B");
        assertThat(answer.getBestAnswerLabel()).isEqualTo("A");
        assertThat(answer.getAnswerLabel()).isEqualTo("B");
    }

    @Test
    public void testSquishEmpty() {
        ResultsJsonMerger merger = new ResultsJsonMerger();

        Results resultsForA = merger.squishIntoResults(exam(), response(""));

        Answer answer = resultsForA.getAnswers().get(0);
        assertThat(resultsForA.getPercentageOfMax()).isEqualTo(0);
        assertThat(answer.getPoints()).isEqualTo(0);
        assertThat(answer.getCorrect()).isFalse();
        assertThat(answer.getFeedback()).isEqualTo("");
        assertThat(answer.getBestAnswerLabel()).isEqualTo("A");
        assertThat(answer.getAnswerLabel()).isEqualTo("");
    }

    private RawResponse response(String responseLabel) {
        RawResponse response = new RawResponse();
        response.setName("bob");
        response.setId(UUID.randomUUID());
        response.setResponses(Collections.singletonMap(questionId, responseLabel));
        return response;
    }

    private RawExam exam() {
        RawExam rawExam = new RawExam();

        RawOption optionA = new RawOption();
        optionA.setFeedback("some feedback");
        optionA.setLabel("A");
        optionA.setPoints(3);
        optionA.setText("option text A");

        RawOption optionB = new RawOption();
        optionB.setFeedback("some feedback for B");
        optionB.setLabel("B");
        optionB.setPoints(1);
        optionB.setText("option text B");

        RawQuestion question = new RawQuestion();
        question.setId(questionId);
        question.setText("question text");
        question.setOptions(Arrays.asList(optionA, optionB));
        rawExam.setQuestions(Collections.singletonList(question));
        return rawExam;
    }
}
