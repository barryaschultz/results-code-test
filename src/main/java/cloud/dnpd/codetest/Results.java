package cloud.dnpd.codetest;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Results {
    private UUID id;
    private String candidateName;
    private String examName;
    private Integer percentageOfMax;
    private List<Answer> answers;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Integer getPercentageOfMax() {
        return percentageOfMax;
    }

    public void setPercentageOfMax(Integer percentageOfMax) {
        this.percentageOfMax = percentageOfMax;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Results results = (Results) o;
        return Objects.equals(id, results.id) &&
                Objects.equals(candidateName, results.candidateName) &&
                Objects.equals(examName, results.examName) &&
                Objects.equals(percentageOfMax, results.percentageOfMax) &&
                Objects.equals(answers, results.answers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, candidateName, examName, percentageOfMax, answers);
    }
}
