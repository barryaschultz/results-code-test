package cloud.dnpd.codetest;

import cloud.dnpd.codetest.jsonfile.JsonFileFetcher;
import cloud.dnpd.codetest.jsonfile.RawExam;
import cloud.dnpd.codetest.jsonfile.RawResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

@Controller
public class ResultsController {

    private final ResultsJsonMerger resultsJsonMerger;
    private final JsonFileFetcher jsonFileFetcher;

    public ResultsController(ResultsJsonMerger resultsJsonMerger, JsonFileFetcher jsonFileFetcher) {
        this.resultsJsonMerger = resultsJsonMerger;
        this.jsonFileFetcher = jsonFileFetcher;
    }

    @GetMapping("/results/{testSessionId}")
    @ResponseBody
    public Results getResultsFor(@PathVariable UUID testSessionId) {
        RawResponse response = jsonFileFetcher.getResponse(testSessionId);
        RawExam exam = jsonFileFetcher.getExam(response.getExam());

        return resultsJsonMerger.squishIntoResults(exam, response);
    }
}
