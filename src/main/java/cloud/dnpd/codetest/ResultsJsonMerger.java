package cloud.dnpd.codetest;

import cloud.dnpd.codetest.jsonfile.RawExam;
import cloud.dnpd.codetest.jsonfile.RawOption;
import cloud.dnpd.codetest.jsonfile.RawQuestion;
import cloud.dnpd.codetest.jsonfile.RawResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class ResultsJsonMerger {

    public Results squishIntoResults(RawExam exam, RawResponse response) {
        //TODO: Squish into results
        Results results = new Results();
        if (exam == null) {
            return results;
        }
        results.setId(response.getId());
        results.setCandidateName(response.getName());
        results.setExamName(exam.getName());
        results.setAnswers(produceAnswers(exam.getQuestions(), response.getResponses()));
        results.setPercentageOfMax(calculatePercentageOfMax(calculateMaxPointsPossible(exam.getQuestions()),
                                                            calculateTotalPointsAchieved(results.getAnswers())));

        return results;
    }


    private List<Answer> produceAnswers(final List<RawQuestion> questions, final Map<UUID, String> responses) {
        List<Answer> answers = new ArrayList<Answer>();
        for (RawQuestion question : questions) {
            answers.add(produceAnswer(responses.get(question.getId()), question));
        }
        return answers;
    }

    private Answer produceAnswer(final String responseLabel, final RawQuestion question) {
        final String emptyString = "";
        final List<RawOption> options = question.getOptions();
        final RawOption bestOption = getBestOption(options);

        Answer answer = new Answer();

        answer.setBestAnswerLabel(bestOption.getLabel());
        answer.setBestAnswerText(bestOption.getText());

        final RawOption chosenOption = getOptionByLabel(options, responseLabel);

        answer.setQuestionText(chosenOption != null ? question.getText() : emptyString);
        answer.setAnswerLabel(chosenOption != null ? chosenOption.getLabel() : emptyString);
        answer.setAnswerText(chosenOption != null ? chosenOption.getText() : emptyString);
        answer.setFeedback(chosenOption != null ? chosenOption.getFeedback() : emptyString);
        answer.setCorrect(chosenOption != null ? chosenOption.getPoints() > 0 : false);
        answer.setPoints(chosenOption != null ? chosenOption.getPoints() : 0);

        return answer;
    }

    private RawOption getOptionByLabel(final List<RawOption> options, final String label) {
        return options.stream()
                .filter(o -> o.getLabel().equals(label))
                .findAny()
                .orElse(null);
    }

    private RawOption getBestOption(final List<RawOption> options) {
        return Collections.max(options, Comparator.comparing(o -> o.getPoints()));
    }

    private Integer calculatePercentageOfMax(final Integer maxPointsPossible, final Integer totalPointsAchieved ){
        final Double achieved = totalPointsAchieved.doubleValue();
        final Double possible = maxPointsPossible.doubleValue();
        final Double result = (achieved / possible) * 100;
        return result.intValue();
    }

    private Integer calculateMaxPointsPossible(final List<RawQuestion> questions) {
        Integer maxPointsPossible = 0;
        for (RawQuestion question : questions) {
            maxPointsPossible += Collections.max(question.getOptions(),
                    Comparator.comparing(q -> q.getPoints())).getPoints();
        }
        return maxPointsPossible;
    }

    private Integer calculateTotalPointsAchieved(List<Answer> answers) {
        Integer totalPointsAchieved = 0;
        for (Answer answer :answers) {
            totalPointsAchieved += answer.getPoints();
        }
        return totalPointsAchieved;
    }
}
