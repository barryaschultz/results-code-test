package cloud.dnpd.codetest.jsonfile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class JsonFileFetcher {

    private final RestTemplate restTemplate;
    private final String examsUrl;
    private final String responseUrl;

    public JsonFileFetcher(
            RestTemplate restTemplate,
            @Value("${resultscodetest.examsurl}") String examsUrl,
            @Value("${resultscodetest.responsesurl}") String responsesUrl
    ) {
        this.restTemplate = restTemplate;
        this.examsUrl = examsUrl;
        this.responseUrl = responsesUrl;
    }

    //TODO: Get the responses from 'responseUrl' and return the one in the array that matches the given ID
    public RawResponse getResponse(UUID id) {
        final List<RawResponse> rawResponses = Arrays.asList(restTemplate.getForObject(responseUrl, RawResponse[].class));

        Optional<RawResponse> optionalRawResponse = rawResponses.stream()
                .filter(response -> response.getId().equals(id))
                .findFirst();

        if (optionalRawResponse.isPresent()) {
            return optionalRawResponse.get();
        } else {
            Logger.getLogger(JsonFileFetcher.class.getName())
                    .log(Level.WARNING, "No Response returned for id " + id);
            return new RawResponse();
        }
    }

    //TODO: Get the exams from 'examsUrl' and return the one in the array that matches the given ID
    public RawExam getExam(UUID id) {

        final Logger logger = Logger.getLogger(JsonFileFetcher.class.getName());
        final ObjectMapper mapper = new ObjectMapper();
        final String responseString = restTemplate.getForObject(examsUrl, String.class);

        Map<String, RawExam> rawExamMap = new HashMap<String, RawExam>();

        if (id != null) {
            try {
                rawExamMap = mapper.readValue(responseString, new TypeReference<HashMap<String, RawExam>>(){});
                final RawExam exam = rawExamMap.get(id.toString());
                if (exam != null) {
                    return exam;
                } else {
                    logger.log(Level.WARNING, "No Exam returned for id " + id);
                }
            } catch (JsonProcessingException ex) {
                logger.log(Level.WARNING, "Problem processing json response for id " + id, ex);
            }
        } else {
            logger.log(Level.WARNING, "Attempt to lookup an Exam with a NULL id");
        }
        return null;
    }
}
