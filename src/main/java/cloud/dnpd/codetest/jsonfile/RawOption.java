package cloud.dnpd.codetest.jsonfile;

import java.util.Objects;

public class RawOption {
    private String label;
    private String text;
    private Integer points;
    private String feedback;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RawOption rawOption = (RawOption) o;
        return Objects.equals(label, rawOption.label) &&
                Objects.equals(text, rawOption.text) &&
                Objects.equals(points, rawOption.points) &&
                Objects.equals(feedback, rawOption.feedback);
    }

    @Override
    public int hashCode() {
        return Objects.hash(label, text, points, feedback);
    }
}
