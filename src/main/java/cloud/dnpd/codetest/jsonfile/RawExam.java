package cloud.dnpd.codetest.jsonfile;

import java.util.List;
import java.util.Objects;

public class RawExam {
    private String name;
    private List<RawQuestion> questions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RawQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<RawQuestion> questions) {
        this.questions = questions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RawExam rawExam = (RawExam) o;
        return Objects.equals(name, rawExam.name) &&
                Objects.equals(questions, rawExam.questions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, questions);
    }
}
