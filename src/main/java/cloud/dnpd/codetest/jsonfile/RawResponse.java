package cloud.dnpd.codetest.jsonfile;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class RawResponse {
    private UUID id;
    private String name;
    private UUID exam;
    private Map<UUID, String> responses;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getExam() {
        return exam;
    }

    public void setExam(UUID exam) {
        this.exam = exam;
    }

    public Map<UUID, String> getResponses() {
        return responses;
    }

    public void setResponses(Map<UUID, String> responses) {
        this.responses = responses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RawResponse that = (RawResponse) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(exam, that.exam) &&
                Objects.equals(responses, that.responses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, exam, responses);
    }
}
