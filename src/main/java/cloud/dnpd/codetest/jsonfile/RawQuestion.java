package cloud.dnpd.codetest.jsonfile;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class RawQuestion {
    private UUID id;
    private String text;
    private List<RawOption> options;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<RawOption> getOptions() {
        return options;
    }

    public void setOptions(List<RawOption> options) {
        this.options = options;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RawQuestion that = (RawQuestion) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(text, that.text) &&
                Objects.equals(options, that.options);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, options);
    }
}
