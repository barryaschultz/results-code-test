package cloud.dnpd.codetest;

import java.util.Objects;

public class Answer {
    private String questionText;
    private String answerLabel;
    private String answerText;
    private Boolean isCorrect;
    private Integer points;

    private String feedback;

    private String bestAnswerLabel;
    private String bestAnswerText;

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getAnswerLabel() {
        return answerLabel;
    }

    public void setAnswerLabel(String answerLabel) {
        this.answerLabel = answerLabel;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Boolean getCorrect() {
        return isCorrect;
    }

    public void setCorrect(Boolean correct) {
        isCorrect = correct;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getBestAnswerLabel() {
        return bestAnswerLabel;
    }

    public void setBestAnswerLabel(String bestAnswerLabel) {
        this.bestAnswerLabel = bestAnswerLabel;
    }

    public String getBestAnswerText() {
        return bestAnswerText;
    }

    public void setBestAnswerText(String bestAnswerText) {
        this.bestAnswerText = bestAnswerText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return Objects.equals(questionText, answer.questionText) &&
                Objects.equals(answerLabel, answer.answerLabel) &&
                Objects.equals(answerText, answer.answerText) &&
                Objects.equals(isCorrect, answer.isCorrect) &&
                Objects.equals(points, answer.points) &&
                Objects.equals(feedback, answer.feedback) &&
                Objects.equals(bestAnswerLabel, answer.bestAnswerLabel) &&
                Objects.equals(bestAnswerText, answer.bestAnswerText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionText, answerLabel, answerText, isCorrect, points, feedback, bestAnswerLabel, bestAnswerText);
    }
}
