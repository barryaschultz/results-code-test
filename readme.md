# Results Squisher

Merges the candidate responses and exam data into a ready to render results.

## Task

Complete the two classes with TODOs in, so that the unit tests pass, and the following URLs return meaningful results when the application is running.

* http://localhost:8080/results/78f0cca4-3f28-49d8-8e60-3d09b132c934
* http://localhost:8080/results/1704cf08-f595-4e52-9a65-ceea7c5ac333
* http://localhost:8080/results/240ebd81-575e-4aba-82fb-d4a6fd67de6b